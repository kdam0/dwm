# Kumar's DWM build
This repo has been created from the `dwm` source repo, and modified with a few patches that serve *my* workflow.

## Requirements
In order to build dwm you need the Xlib header files for your distro.

## Installation
```
make clean install
```
Now you can either use `exec dwm` at the end of your `.xinitrc` and it should start up `dwm` when you do `startx`. I like to use a gui greeter because it allows you to pick any entry in `/usr/share/xsessions/`at login. So just create a new entry for `dwm` here `/usr/share/xsessions/dwm.desktop`:
```
[Desktop Entry]
Name=dwm
Encoding=UTF-8
Comment=Runs dwm
Exec=dwm
Type=Application
```

## Configuration
The configuration of dwm is done by creating a custom config.h and (re)compiling the source code.

## Patches
You can find all the patches used in the `patches/` dir. All of these can be found in the patches page in the official dwm website.

- autostart (place a file called `autostart.sh` in your `$XDG_CONFIG_HOME/dwm` dir)
- vanitygaps
- pertag
- scratchpad
- always centered
- restartsig
